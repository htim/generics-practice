﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ulearnGenerics
{
    class Program
    {
        static void Main(string[] args)
        {
            var processor = Processor.CreateEngine<MyEngine>().For<MyEntity>().With<MyLogger>();

            var repository = new Repository();
            repository.Save(new MyEntity());
            repository.Save(new MyEngine());
            repository.Save(new MyLogger());

            var pairs = repository.FindByType<MyLogger>();
            var obj = repository.FindById<MyLogger>(pairs.Where(x => x.Item2 is MyLogger).Select(x => x.Item1).FirstOrDefault());
            var a = 0;
        }
    }
}
