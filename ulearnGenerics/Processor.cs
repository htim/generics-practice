﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ulearnGenerics
{

    public interface IProcessorBuilder<TEngine>
    {
        IProcessorBuilder<TEngine, TEntity> For<TEntity>();
    }

    public interface IProcessorBuilder<TEngine, TEntity>
    {
        Processor<TEngine, TEntity, TLogger> With<TLogger>();
    }
    
    public class ProcessorBuilder<TEngine> : IProcessorBuilder<TEngine>
    {
        public IProcessorBuilder<TEngine, TEntity> For<TEntity>()
        {
            return new ProcessorBuilder<TEngine, TEntity>();
        }
    }

    public class ProcessorBuilder<TEngine, TEntity> : IProcessorBuilder<TEngine, TEntity>
    {
        public Processor<TEngine, TEntity, TLogger> With<TLogger>()
        {
            return new Processor<TEngine, TEntity, TLogger>();
        }
    }

    public class Processor
    {
       public static IProcessorBuilder<TEngine> CreateEngine<TEngine>()
        {
            return new ProcessorBuilder<TEngine>();
        }
    }

    public class Processor<TEngine, TEntity, TLogger>
    {
        
    }


}
