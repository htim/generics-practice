﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ulearnGenerics
{
    public interface IRepository<ID>
    {
        T Save<T>(T t);
        IEnumerable<Tuple<ID, T>> FindByType<T>();
        T FindById<T>(ID id);
    }

    class Repository : IRepository<Guid>
    {
        private IDictionary<Guid, object> repo;

        public Repository()
        {
            repo = new Dictionary<Guid, object>();
        }

        public T FindById<T>(Guid id)
        {
            return
                repo
                    .Where(x => x.Key == id && x.Value is T)
                    .Select(x => (T) x.Value)
                    .FirstOrDefault();
        }

        public IEnumerable<Tuple<Guid, T>> FindByType<T>()
        {
            return
                repo
                    .Where(x => x.Value is T)
                    .Select(x => new Tuple<Guid, T>(x.Key, (T)x.Value));
        }

        public T Save<T>(T t)
        {
            repo.Add(Guid.NewGuid(), t);
            return t;
        }

    }
}
